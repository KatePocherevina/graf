// ConsoleApplication2.cpp: ���������� ����� ����� ��� ����������� ����������.
//
#include <iostream>
#include <string.h>
#include <stdio.h>
#include "stdafx.h"
#include "GL/glew.h"
#include "GL/freeglut.h"
#include <iostream>

GLuint VBO;

static void RenderScene();
static void CreateVertexBuffer();

const GLchar* vertexShaderSource = "#version 330 \n"
"layout (location = 0) in vec3 position;\n"
"void main()\n"
"{\n"
"gl_Position = vec4(position.x, position.y, position.z, 1.0);\n"
"}\0";
const GLchar* fragmentShaderSource = "#version 330 \n"
"out vec4 color;\n"
"void main()\n"
"{\n"
"color = vec4(1.0f, 0.0f, 0.0f, 1.0f);\n"
"}\n\0";

int main(int argc, char** argv)
{
	glutInit(&argc, argv);
	//����� ����������� ����������(����� � ���� � 2�� ��������, ����� ��������� �������� � ����� � ����� ������� � �������������)
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);
	// ��������� ���������� ����
	glutInitWindowSize(1024, 768);
	//��������� ������������ �������� ������ ���� ������
	glutInitWindowPosition(100, 100);
	//�������� � ���������
	glutCreateWindow("Test");
	//������� ��� ����������� ����������� ���� (������� ������� �������)
		glutDisplayFunc(RenderScene);
	//������������� � �������� �� ������
	GLenum res = glewInit();
	if (res != GLEW_OK) {
		std::cerr << "Error: "<< glewGetErrorString(res) ;
			return 1;
	}
	//������������� ������� ����  (���� ������� ������)
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	CreateVertexBuffer();
	//���� � ������� ���� ���� (�������� ������� �� ������� �������)
	glutMainLoop();
	//�������� VBO � �������� ���������� ���������� ��� �������� ��������� �� ����� ������
	GLuint VBO; 
	return 0;
}
//C:\\Users\\���������\\Desktop\\����.�������\\ConsoleApplication2\\vertexshader

static void AddShader(GLuint ShadeProgram, const char* pShaderText, GLenum ShaderType)
{
	//�� �������� ������� ���������� �������� ����� �������� ������������ �������
	GLuint vertexShader;
	vertexShader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertexShader, 1, &vertexShaderSource, NULL);
	glCompileShader(vertexShader);
	// Check for compile time errors
	GLint success;
	GLchar infoLog[512];
	glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success);

	if (!success)
	{
		glGetShaderInfoLog(vertexShader, 512, NULL, infoLog);
		std::cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << infoLog << std::endl;
	}
	GLuint fragmentShader;
	fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragmentShader, 1, &fragmentShaderSource, NULL);
	glCompileShader(fragmentShader);
	// Check for compile time errors
	glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &success);
	if (!success)
	{
		glGetShaderInfoLog(fragmentShader, 512, NULL, infoLog);
		std::cout << "ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n" << infoLog << std::endl;
	}
	// Link shaders
	GLint shaderProgram = glCreateProgram();
	glAttachShader(shaderProgram, vertexShader);
	glAttachShader(shaderProgram, fragmentShader);
	glLinkProgram(shaderProgram);
	// Check for linking errors
	glGetProgramiv(shaderProgram, GL_LINK_STATUS, &success);
	if (!success) {
		glGetProgramInfoLog(shaderProgram, 512, NULL, infoLog);
		std::cout << "ERROR::SHADER::PROGRAM::LINKING_FAILED\n" << infoLog << std::endl;
	}
	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);
}

//�������� ������ ������
static void CreateVertexBuffer(){
	GLfloat vertices[] = {
		-0.5f, -0.5f, 0.0f,
		0.5f, -0.5f, 0.0f,
		0.0f,  0.5f, 0.0f
	};
	//������� 1 ������ �  ������ ������
	glGenBuffers(1, &VBO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices) , vertices, GL_STATIC_DRAW);
	//glBufferData(GL_ARRAY_BUFFER, sizeof(Vertices) + sizeof(Vertices2), (Vertices, Vertices2), GL_STATIC_DRAW);
}
//�������, ���������� �� ��������� � ����
static void RenderScene(){
	//������� ������
	glClear(GL_COLOR_BUFFER_BIT);
	// ���������� ������������� ��������� ������� � �������� 0 � ��������� 
	glEnableVertexAttribArray(0);
	//������� ����������� �����, ������������� ��� ��� ��������� 
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	//��������� ��������, ��� ������������ ������ ������
	//0- ������ ���������
	//3- ���-�� ��������� (�������)
	//GL_FLOAT � ��� ������� ���������� 
	//GL_FALSE - �� �� ����� ������������� �������� ����� �������������� � ��������� 
	//0 � ���� �� ���������� ������ �������� (���������� ���������� ������� � ������� , �� ���� �������� ������ � ������ 
	//0 � �������� � ���������
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	// ������� ��� ��������� ��������� �����������, 3 �������, ������� � 0-�� � ������ ������
	glDrawArrays(GL_TRIANGLES, 0, 3);
	glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glDrawArrays(GL_TRIANGLES, 3, 3);
	// ��������� ������ ������� �������, ��� ������ �������� ������������� � ���
	glDisableVertexAttribArray(0);
	//�������� ������� ����� � ����� ����� �������
	glutSwapBuffers();
}


